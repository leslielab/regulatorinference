

########### Grid search for best set of parameters using
## crossvalidation for tree-based group lasso

#' Tree-based group lasso is used in the group model to share information
#' across different samples of the subtype. The best parameters are
#' estimated by grid search.
#'
#' @title Crossvalidation for group model
#' @param X Feature matrix.
#' @param Y Gene expression change.
#' @param subtypes Named vector representing the subtypes of all samples.
#' @param sample.specific.features List of matrices representing sample specific features.
#' @param init.wvec Initial w vector matrix (Models for all samples).
#' @param sample.specific.exclusion List of features to be excluded for each sample.
#' @param cv.folds Crossvalidation folds.
#' @param lambda.list Vector of regularization parameters.
#' @param node.weight.list Vector of node weight parameters. See \code{Details}.
#' @param out.file Output file name.
#' @param no.threads Number of threads for parallel processing. Default is \code{1}.
#' @param test Logical for testing. If \code{TRUE} the inference will be run for only one sample.
#' @details Combinations of \code{lambda.list} values and \code{node.weight.list}
#' are generated to create the parameter grid. Same node weights are used
#' for nodes of each subtype.
#' @return   A list is saved and also returned with the following items
#'  \item{error.list }{Test errors across all samples for each parameter setting}
#'  \item{lambda.list }{Regularization parameters}
#'  \item{node.weight.list }{Node weights in each setting}
#'  \item{early.termination }{Logical indicating if any of the folds
#'    terminated early because of mathematical issues}

#' @author Maintainer: Manu Setty <manu@@cbio.mskcc.org>
#' @seealso \code{\link{create.inputs}}
#' @export


group.crossvalidation <- function (X, Y, subtypes, sample.specific.features=NULL, init.wvec,
                                   subtype.exclusion.features=NULL, cv.folds, lambda.list, node.weight.list,
                                   out.file='group_crossvalidation_results.rda', no.threads=1, test=FALSE,
                                   cached.immutables=FALSE, XTX.global=NULL, XTY.global=NULL) {

  global.start <- get.time ()
  ## ###################################################################################
  ## Error checking
  if (nrow (X) != nrow (Y))
    stop ("Feature matrix (X) and value matrix (Y) do not have the same number of genes")
  
  ## Dimensions of sample.specific.features
  for (i in labels (sample.specific.features))
    if (!all (dim (sample.specific.features) == dim (X)))
      stop (sprintf ("Incompatible dimensions between X and %s component of sample.specific.features", i))

  ## Crossvalidation folds
  if (length (setdiff (1:nrow (X), unlist (cv.folds))) != 0 )
    warning ("Cross validation folds do not contain all the observations")

  ## Create parameter grid with all combination of lambda.list and node.weight.list
  parameter.grid <- permutations (length (node.weight.list), 2, node.weight.list, repeats.allowed=FALSE)
  parameter.grid <- cbind (lambda.list, matrix (rep (parameter.grid, each=length (lambda.list)), ncol=2))
  ## Use the same parameters for all subtypes
  parameter.grid <- cbind (parameter.grid, parameter.grid[, rep (3, length (unique (subtypes))-1)])
  show (sprintf ("No. of entries in parameter grid: %d", nrow (parameter.grid)))
  
  error.list <- rep (0, nrow (parameter.grid))

  ## ###################################################################################
  ## Cross validation thread. Run the group lasso for each fold
  ## and compute held out test error
  cv.thread <- function (ind) {
    
    error <- Inf
    early.termination <- TRUE
    show (sprintf ('Crossvalidation for parameter set: %d', ind))
    time.start <- get.time ()

    ## Iteration specific parameters
    lambda <- parameter.grid[ind,1]
    node.weight <- parameter.grid[ind, 2:ncol (parameter.grid)]
    prediction.matrix <- matrix (0, nrow (Y), ncol(Y), dimnames=dimnames (Y))
    
    ## Some initializations
    SubtypeEF <- NULL
    SamplesSF <- NULL 
    
    if (length (subtype.exclusion.features) != 0) 
      SubtypeEF <- subtype.exclusion.features
    
    for (i in 1:length (cv.folds)) {
      fold <- cv.folds[[i]]
      if (length (sample.specific.features) != 0)
        SamplesSF <- lapply (sample.specific.features, function (x) {x[-fold,]} )
      res <- tree.group.lasso (X[-fold,], Y[-fold,], lambda, node.weight, subtypes=subtypes,
                               sample.specific.features=SamplesSF, subtype.exclusion.features=SubtypeEF,
                               init.beta=init.wvec, cached.immutables=cached.immutables, fold=i,
                               XTX.global=XTX.global, XTY.global=XTY.global)
      early.termination <- early.termination & res$early.termination
      
      if (length (sample.specific.features) != 0)
        SamplesSF <- lapply (sample.specific.features, function (x) {x[fold,]} )
      prediction.matrix[fold,] <- predictions (X=X[fold,], sample.specific.features=SamplesSF,
                                               subset.matrix=SubtypeEF$matrix, beta=res$beta)
    }
  
    error <- sum (colSums((Y - prediction.matrix)^2)/nrow (X))
    time.end <- get.time ()
    show (sprintf ('Time to run param set %d: %0.1f', ind, (time.end-time.start)/60))

    return (list (error.list=error,
                  early.termination=early.termination))
  }

  ## ###################################################################################
  ## Run crossvalidation + packaging
  if (test) {
    inputs <- 1
  } else {
    inputs <- 1:nrow (parameter.grid)
  }

  if (no.threads == 1) {
    for (input in inputs) {
      if (input == 1) {
        solution <- cv.thread (input)
      } else {
        solution <- merge.lists (solution, cv.thread (input))
      }
    }
  } else {
    registerDoMC (no.threads)
    solution <- foreach (input=inputs, .combine=merge.lists) %dopar% cv.thread (input)
  }

  solution$parameter.grid <- parameter.grid
  save (solution, file=out.file)

  global.end <- get.time ()
  show (sprintf ("Minutes to run crossvalidation : %0.1f", (global.end - global.start)/60))
  invisible (solution)
}
