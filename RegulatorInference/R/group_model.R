
########### Crossvalidation + Beta for given parameters
#' Tree-based group lasso is used in the group model to share information
#' across different samples of the subtype. Regression models are learnt
#' for the specified parameter setting.
#'
#' @title Group lasso regression models
#' @param X Feature matrix.
#' @param Y Gene expression change.
#' @param sample.specific.features List of matrices representing sample specific features.
#' @param sample.specific.exclusion List of features to be excluded for each sample.
#' @param cv.folds Crossvalidation folds.
#' @param lambda Regularization parameter.
#' @param node.weight Node weight parameters. See \code{Details}.
#' @param out.file Output file name.
#' @param cached.immutables Logical indicating if XTX and XTY will be precomputed.
#' If set to \code{TRUE}, \code{XTX.global} and \code{XTY.global} should be specified.
#' @param XTX.global Precomputed \code{X'X} for all folds
#' @param XTY.global Precomputed \code{X'Y} for all folds
#' @details Determine train and test correlations, regression models for the given
#' parameter setting. The \code{node.weight} parameter should representing
#' weights for the root node and each of the subtype nodes.
#' @return    A list is saved and also returned with the following items
#'  \item{train.correlations }{Training correlations}
#'  \item{train.errors }{Training errors}
#'  \item{test.correlations }{Test correlations}
#'  \item{test.errors }{Test errors}
#'  \item{wvec }{Matrix of regression coefficients for all regulators and samples}
#'  \item{ypre }{Gene expession change prediction for all genes and samples}
#' @author Maintainer: Manu Setty <manu@@cbio.mskcc.org>
#' @seealso \code{\link{create.inputs}}, \code{\link{group.crossvalidation}}
#' @export


group.models <- function (X, Y, subtypes, sample.specific.features=NULL, init.wvec,
                          subtype.exclusion.features=NULL, cv.folds, lambda, node.weight,
                          out.file='group_regression_models.rda',
                          cached.immutables=FALSE, XTX.global=NULL, XTY.global=NULL) {

  time.start <- get.time ()
  ## ###################################################################################
  ## Error checking
  if (nrow (X) != nrow (Y))
    stop ("Feature matrix (X) and value matrix (Y) do not have the same number of genes")
  
  ## Dimensions of sample.specific.features
  for (i in labels (sample.specific.features))
    if (!all (dim (sample.specific.features) == dim (X)))
      stop (sprintf ("Incompatible dimensions between X and %s component of sample.specific.features", i))

  ## Crossvalidation folds
  if (length (setdiff (1:nrow (X), unlist (cv.folds))) != 0 )
    warning ("Cross validation folds do not contain all the observations")

  ## Regularization parameters
  if (length (node.weight) != length (unique (subtypes)) + 1)
    stop ("node.weight parameter should contain weights for length (subtypes) + 1 nodes")
  
  prediction.matrix <- matrix (0, nrow (Y), ncol(Y), dimnames=dimnames (Y))

  ## Some initializations
  SubtypeEF <- NULL
  SamplesSF <- NULL
  
  if (length (subtype.exclusion.features) != 0) 
    SubtypeEF <- subtype.exclusion.features
  
  ## Test correlations and errors
  show ('Determining test performance....')
  for (i in 1:length (cv.folds)) {
    fold <- cv.folds[[i]]
    if (length (sample.specific.features) != 0)
      SamplesSF <- lapply (sample.specific.features, function (x) {x[-fold,]} )
    res <- tree.group.lasso (X[-fold,], Y[-fold,], lambda, node.weight, subtypes=subtypes,
                             sample.specific.features=SamplesSF, subtype.exclusion.features=SubtypeEF,
                             init.beta=init.wvec, cached.immutables=cached.immutables, fold=i,
                             XTX.global=XTX.global, XTY.global=XTY.global)
    
    if (length (sample.specific.features) != 0)
      SamplesSF <- lapply (sample.specific.features, function (x) {x[fold,]} )
    prediction.matrix[fold,] <- predictions (X=X[fold,], sample.specific.features=SamplesSF,
                                             subset.matrix=SubtypeEF$matrix, beta=res$beta)
  }
  test.correlations <- diag (cor (Y, prediction.matrix[, colnames (Y)], method='spearman'))
  test.errors <- (Y - prediction.matrix)^2/nrow (Y)
  
  ## Training performance + wvec determination
  show ('Determining group models.... ')
  if (length (sample.specific.features) != 0)
    SamplesSF <- sample.specific.features
  res <- tree.group.lasso (X, Y, lambda, node.weight, subtypes=subtypes,
                           sample.specific.features=SamplesSF, subtype.exclusion.features=SubtypeEF,
                           init.beta=init.wvec, cached.immutables=cached.immutables, fold=11,
                           XTX.global=XTX.global, XTY.global=XTY.global)
  
  prediction.matrix <- predictions (X=X, sample.specific.features=SamplesSF,
                                    subset.matrix=SubtypeEF$matrix, beta=res$beta) 

  train.errors <- colSums((Y - prediction.matrix)^2)/nrow (X)
  train.correlations <- diag (cor (Y, prediction.matrix[, colnames (Y)], method='spearman'))

  ## Package the solution
  solution <- list (wvec=res$beta,train.errors=train.errors, train.correlations=train.correlations,
                    ypre=prediction.matrix, test.errors=test.errors, test.correlations=test.correlations,
                    lambda=lambda, node.weight=node.weight)
  save (solution, file=out.file)

  time.end <- get.time ()
  show (sprintf ('Time for analyzing parameters: %.2f', (time.end - time.start)/60))
  invisible (solution)
  
}
