\name{RegulatorInference-package}
\alias{RegulatorInference-package}
\alias{RegulatorInference}
\docType{package}
\title{
Regulator Inference in Cancer  
}
\description{
  Regulator Inference in Cancer
}
\details{
\tabular{ll}{
Package: \tab RegulatorInference\cr
Type: \tab Package\cr
Version: \tab 1.0\cr
Date: \tab 2011-04-20\cr
License: \tab GPL (>= 2)\cr
LazyLoad: \tab yes\cr
}
}
\author{
Manu Setty
Maintainer: Manu Setty <manu@cbio.mskcc.org>
}
\references{
~~ Literature or other references for background information ~~
}
