\name{normalize.mir.exprs}
\alias{normalize.mir.exprs}
\title{Normalize microRNA expression data}
\usage{
  normalize.mir.exprs(mir.dir, sample.mapping.file,
    out.file)
}
\arguments{
  \item{mir.dir}{Directory containing the microRNA
  expression files. All files with extension \code{txt} in
  this directory are considered for normalization.}

  \item{sample.mapping.file}{File containing mapping from
  file name to sample name. The file should be
  tab-separated and contain two columns. First column
  contains file names and second column contains the sample
  name.}

  \item{out.file}{rda file for normalized data.}
}
\value{
  A matrix of normalized gene exprssion.
}
\description{
  This function uses \code{AgiMicroRna} package to
  normalize microRNA expression data.
}
\details{
  This function reads in all the cel files in
  \code{cel.dir}. The data is then background corrected
  using \code{RMA} and qunatile normalized. The probes
  representing the same miRs are median summarized to
  obtain miR level data.
}
\author{
  Maintainer: Manu Setty <manu@cbio.mskcc.org>
}
\seealso{
  \code{\link{readMicroRnaAFE}}, \code{\link{rmaMicroRna}}
}

