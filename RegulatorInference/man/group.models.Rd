\name{group.models}
\alias{group.models}
\title{Group lasso regression models}
\usage{
  group.models(X, Y, subtypes,
    sample.specific.features = NULL, init.wvec,
    subtype.exclusion.features = NULL, cv.folds, lambda,
    node.weight, out.file = "group_regression_models.rda",
    cached.immutables = FALSE, XTX.global = NULL,
    XTY.global = NULL)
}
\arguments{
  \item{X}{Feature matrix.}

  \item{Y}{Gene expression change.}

  \item{sample.specific.features}{List of matrices
  representing sample specific features.}

  \item{sample.specific.exclusion}{List of features to be
  excluded for each sample.}

  \item{cv.folds}{Crossvalidation folds.}

  \item{lambda}{Regularization parameter.}

  \item{node.weight}{Node weight parameters. See
  \code{Details}.}

  \item{out.file}{Output file name.}

  \item{cached.immutables}{Logical indicating if XTX and
  XTY will be precomputed. If set to \code{TRUE},
  \code{XTX.global} and \code{XTY.global} should be
  specified.}

  \item{XTX.global}{Precomputed \code{X'X} for all folds}

  \item{XTY.global}{Precomputed \code{X'Y} for all folds}
}
\value{
  A list is saved and also returned with the following
  items \item{train.correlations }{Training correlations}
  \item{train.errors }{Training errors}
  \item{test.correlations }{Test correlations}
  \item{test.errors }{Test errors} \item{wvec }{Matrix of
  regression coefficients for all regulators and samples}
  \item{ypre }{Gene expession change prediction for all
  genes and samples}
}
\description{
  Tree-based group lasso is used in the group model to
  share information across different samples of the
  subtype. Regression models are learnt for the specified
  parameter setting.
}
\details{
  Determine train and test correlations, regression models
  for the given parameter setting. The \code{node.weight}
  parameter should representing weights for the root node
  and each of the subtype nodes.
}
\author{
  Maintainer: Manu Setty <manu@cbio.mskcc.org>
}
\seealso{
  \code{\link{create.inputs}},
  \code{\link{group.crossvalidation}}
}

