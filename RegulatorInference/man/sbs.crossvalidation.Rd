\name{sbs.crossvalidation}
\alias{sbs.crossvalidation}
\title{Crossvalidation for sample-by-sample model}
\usage{
  sbs.crossvalidation(X, Y,
    sample.specific.features = NULL,
    sample.specific.exclusion = NULL, cv.folds,
    out.file = "sbs_crossvalidation_results.rda",
    no.threads = 1, test = FALSE)
}
\arguments{
  \item{X}{Feature matrix.}

  \item{Y}{Gene expression change.}

  \item{sample.specific.features}{List of matrices
  representing sample specific features.}

  \item{sample.specific.exclusion}{List of features to be
  excluded for each sample.}

  \item{cv.folds}{Crossvalidation folds.}

  \item{out.file}{Output file name.}

  \item{no.threads}{Number of threads for parallel
  processing. Default is \code{1}.}

  \item{test}{Logical for testing. If \code{TRUE} the
  inference will be run for only one sample.}
}
\value{
  A list is saved and also returned with the following
  items \item{correlations }{Test correlations}
  \item{lambda.list }{lambda parameter for each sample}
  \item{error.list}{Squared error}
}
\description{
  \code{glment} package is used to perform crossvalidation
  for each sample to determine the \code{lambda} parameter
  and test correlations.
}
\details{
  Crossvalidation is performed on a sample-by-sample basis
  to determine \code{lambda} value with least squared
  error. Test correlations are determined using this
  parameter for regularization.
}
\author{
  Maintainer: Manu Setty <manu@cbio.mskcc.org>
}
\seealso{
  \code{\link{create.inputs}}
}

