\name{create.inputs}
\alias{create.inputs}
\title{
Create inputs for regulator inference
}
\description{
Function to read in the different types of data and create a rda file
containing all the necessary inputs for sample-by-sample model.
}
\usage{
create.inputs(sample.annotation.file, genes.exprs.file,
core.features.files, mirs.exprs.file = "", sample.specific = list(),
out.file, ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{sample.annotation.file}{
Sample annotations or subtypes. The file should be tab-separated and
contain two columns. First column contains sample names and second
column contains the subtype. See \code{Details}.
}
  \item{genes.exprs.file}{
Gene expression file (txt or rda).
}
  \item{core.features.files}{
Named list of core features (TFs/ miRs) files (txt or rda).
}
  \item{mirs.exprs.file}{
Optional. miR expression file (txt or rda).
}
  \item{sample.specific}{
Optional. Named list of sample specific features (copy numbers/ DNA
methylation) files (txt or rda).
}
  \item{out.file}{
File to save all the inputs.
}
  \item{...}{
Optional arguments. See \code{Details}.
}
}
\details{
Atleast one reference has to be specified in the
\code{sample.annotation.file}. This can be done using \code{REFERENCE}
as subtype.

The txt or rda files are automatically detected and loaded using
\code{load.txt.rda}. The rda files should contain a single object called
\code{matrix} with appropriate row names and sample names as column
names. txt files should be tab separated, first column should represent
gene/miR names and first row should represent the sample names.

Gene expression change and miR exclusion are based on samples annotated
as \code{REFERENCE}.

The core features are TFs and miRs. microRNA families will be used for
determining seed matches in 3'UTR. However, the miR expression data is
usually available for individual miRs. Hence optionally, an argument
\code{mirs.family} can be specified to map individual microRNAs to their
families. The parameter points to a text or rda file. Rows represent the
families and columns represent the individual miRs.

This function might throw a \code{warning} saying
\code{data length is not a multiple of split variable}. This can be ignored.
}
\value{
  The following objects will be saved in the output file
  \item{X }{Feature matrix}
  \item{Y }{Value matrix}
  \item{sample.specific.features }{List of matrices representing sample
    specific features}
  \item{exclude.list }{List of features to be excluded for each sample}
  \item{cv.folds }{Crossvalidation folds. Default is 10 folds}
  \item{subtypes }{Named vector of sample subtypes}
  \item{subtype.exclusion.features }{Subtype specific exclusion of
    features [For Group model]}
  \item{init.wvec }{Initial w vectors[For Group model]}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Maintainer: Manu Setty <manu@cbio.mskcc.org>
}

\seealso{
\code{\link{load.txt.rda}}
}
