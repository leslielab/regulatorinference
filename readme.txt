RegulatorInference is a package for inferring subtype specific and common candidate regulators which best explain gene expression dysregulation in cancer compared to normal samples.


Requirements
1. R version (2.13.0 or later)
2. glmnet: package for solving lasso and elastic-net regularized gloms. http://cran.r-project.org/web/packages/glmnet/index.html
3. doMC: package for parallel processing. http://cran.r-project.org/web/packages/doMC/index.html
4. gplots: package for creating heatmaps

Installation
1. Make sure all the dependent packages are installed (2-3 in requirements).
2. Type "R CMD INSTALL RegulatorInference.tar.gz" from the command line. This should install the package.

Usage
Step 1: Data preprocessing
Expression data has to be normalized before use. Affymetrix gene expression  data can be normalized using normalize.gene.exprs function. This function uses affy bioconductor package. The normalized data will be saved in a Rdata file. Similarly miRNA expression data can be normalized using normalize.mir.exprs function. AgiMicrorna package has to be installed for this. We use RMA normalization in both cases.

Step 2: Creating Rdata inputs file for RegulatorInference
Use create.inputs to create a Rdata file that can be used as input for learning regression models. 

Step 3: Sample-by-sample models
Function sbs.run.regulator.inference runs the whole pipeline for sample-by-sample model including crossvalidation (sbs.crossvalidation), regression model inference (sbs.models) and determination of candidate regulators (candidate.regulators). The output files sbs_crossvalidation_results.rda and sbs_regression_models.rda contain the results of crossvalidation and model inference respectively. Model_clustering.pdf shows the unsupervised hierarchical clustering of regression models, Agg_errors.pdf shows the scoring distribution and cutoff used for determination of candidate regulators and Candidate_regulators.txt contains the subtype specific and common candidate regulators. The subtype regulators are sorted based on their scores.

Step 3: Group models
group.run.regulator.inference function is the analogous wrapper for group model inference. This function needs an additional parameter init.wvec which specifies the starting model coefficient. The sample models can be used to initialize the models. The crossvalidation is performed by grid search. lambda.list is a vector of regularization parameters and node.weight.list is a vector of values < 1 which define the node weights in the tree. It is strongly advised to find a neighborhood for lambda before optimizing over node weights. group_crossvalidation_results.rda and group_regression_models.rda are the results of crossvalidation and model inference respectively. candidate.regulators function is again used to produce Model_clustering.pdf, Agg_errors.pdf and Candidate_regulators.txt. 



Sample data
1. tfs_mirs.txt: TF and miRNA hits for genes
2. gene_expression.txt: Normalized gene expression for 3 proneural, mesenchymal and normal samples from TCGA GBM dataset.
3. copy_numbers.txt: Gene copy numbers for the tumor samples from TCGA GBM.
4. annotations.txt: Sample annotations
5. sample_inputs.rda: Rdata file after running create.inputs
6. Agg_errors.pdf, Model_clustering.pdf, Candidate_regulators.txt: Sample-by-sample results for the data.

Command for creating sample_inputs.rda: create.inputs ('annotations.txt', 'gene_expression.txt', list(TFmiRNAs='tfs_mirs.txt'), NULL, list (CopyNumbers='copy_numbers.txt'), 'sample_inputs.rda')
Command for determining sample-by-sample models : sbs.run.regulator.inference ('sample_inputs.rda', '.', no.threads=3)
