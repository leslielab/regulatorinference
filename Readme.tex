\documentclass[10pt]{article}
\usepackage{authblk}
\usepackage{hyperref}

\title{RegulatorInference package for inferring regulators driving expression dysregulation in tumors}
\author{Manu Setty: manu@cbio.mskcc.org}
\affil{Computational Biology Program, Memorial Sloan-Kettering Cancer Center, New York, NY}

\addtolength{\oddsidemargin}{-0.5in}
\addtolength{\topmargin}{-0.5in}
\addtolength{\textwidth}{1in}
\addtolength{\textheight}{1in}


\begin{document}
\maketitle
RegulatorInference is a package for inferring subtype specific and common candidate regulators which best explain gene expression dysregulation in cancer compared to normal samples.

\section{Requirements}
\begin{enumerate}
\item R: version 2.15.0 or later.
\item glmnet: package for solving lasso and elastic-net regularized gloms.  \href{http://cran.r-project.org/web/packages/glmnet/index.html}{Link}.
\item doMC: package for parallel processing. \href{http://cran.r-project.org/web/packages/doMC/index.html}{Link}.
\item gplots: package for creating heatmaps. \href{http://cran.r-project.org/web/packages/gplots/index.html}{Link}.
\item MASS: package used for determining matrix inverses. \href{http://cran.r-project.org/web/packages/MASS/index.html}{Link}.
\item affy (Optional): package for normalizing gene expression data. \href{http://www.bioconductor.org/packages/2.8/bioc/html/affy.html}{Link}.
\item AgiMicorna (Optional): package for normalizing miRNA expression data. \href{http://www.bioconductor.org/packages/2.8/bioc/html/AgiMicroRna.html}{Link}. 
\end{enumerate}

\section{Installation}
\begin{enumerate}
\item Make sure all the dependent packages are installed (2-5 in requirements).
\item Type \texttt{R CMD INSTALL RegulatorInference\_$<$version$>$.tar.gz} from the command line. This should install the package.
\end{enumerate}
\subsection{Testing the installation}
\begin{enumerate}
\item Download the sample data from \href{http://cbio.mskcc.org/leslielab/RegulatorInference/sample\_data.tar.gz}{http://cbio.mskcc.org/leslielab/RegulatorInference/sample\_data.tar.gz}.
\item Load the package using the command \texttt{library (RegulatorInference)}.
\item Run the command \texttt{regulatorinference.test.installation \linebreak  ($<$path.to.sample.data$>$)} where \texttt{$<$path.to.sample.data$>$} is the directory containing the sample data (See below).  

This  will create the following output files: \texttt{sample\_inputs.rda}, \linebreak \texttt{Agg\_errors.pdf}, \texttt{Model\_clustering.pdf}, \texttt{Candidate\_regulators.txt}. See below for description of these files.
\end{enumerate}


\section{Usage}
\subsection{Data preprocessing}
Expression data has to be normalized before use. Affymetrix gene expression  data can be normalized using \texttt{normalize.gene.exprs} function. This function uses \href{http://www.bioconductor.org/packages/2.8/bioc/html/affy.html}{affy} bioconductor package. The normalized data will be saved in a Rdata file. Similarly miRNA expression data can be normalized using \linebreak \texttt{normalize.mir.exprs} function.  \href{http://www.bioconductor.org/packages/2.8/bioc/html/AgiMicroRna.html}{AgiMicrorna} package has to be installed for this. We use RMA normalization in both cases.

\subsection{Creating Rdata inputs file for RegulatorInference}
Use \texttt{create.inputs} to create a Rdata file that can be used as input for learning regression models. 

\subsection{Sample-by-sample models}
Function \texttt{sbs.run.regulator.inference} runs the whole pipeline for sample-by-sample model including crossvalidation (\texttt{sbs.crossvalidation}), regression model inference (\texttt{sbs.models}) and determination of candidate regulators \linebreak (\texttt{candidate.regulators}). The output files \texttt{sbs\_crossvalidation\_results.rda} and \texttt{sbs\_regression\_models.rda} contain the results of crossvalidation and model inference respectively. \texttt{candidata.regulators} produces the following outputs:
\begin{itemize}
\item \texttt{Model\_clustering.pdf}: Plot of unsupervised hierarchical clustering of regression models.
\item \texttt{Agg\_errors.pdf}: Plot showing the score distribution and cutoff used for determination of candidate regulators 
\item \texttt{Candidate\_regulators.txt}: Tab separated file with subtype specific and common candidate regulators and their predicted direction of regulation. The subtype regulators are sorted based on their scores.
\end{itemize}

\subsection{Group models}
\texttt{group.run.regulator.inference} function is the analogous wrapper for group model inference. This function needs an additional parameter \texttt{init.wvec} which specifies the starting model coefficient. The sample models can be used to initialize the models. 

The crossvalidation is performed by grid search. Parameter \texttt{lambda.list} is a vector of regularization parameters and \texttt{node.weight.list} is a vector of values $< 1$ which define the node weights in the tree. It is strongly advised to find a neighborhood for \texttt{lambda} before optimizing over node weights. 

\texttt{group\_crossvalidation\_results.rda} and \texttt{group\_regression\_models.rda} are the results of crossvalidation and model inference respectively. \linebreak \texttt{candidate.regulators} function is again to do feature analysis to identify candidate regulators

\section {Sample data}
\subsection {Input files}
\begin{enumerate}
\item \texttt{tfs\_mirs.txt}: TF and miRNA hits for genes.
\item \texttt{gene\_expression.txt}: Normalized gene expression for 3 proneural, mesenchymal and normal samples from TCGA GBM dataset.
\item \texttt{copy\_numbers.txt}: Gene copy numbers for the tumor samples from TCGA GBM.
\item \texttt{annotations.txt}: Sample annotations.
\item \texttt{sample\_inputs.rda}: Rdata file after running \texttt{create.inputs}.
\end{enumerate}

\subsection {Output files}
\begin{enumerate}
\item \texttt{sbs\_crossvalidation\_results.rda}: Rdata file with sample-by-sample crossvalidation results.
\item \texttt{sbs\_regression\_models.rda}: Rdata file with sample-by-sample model inference results.
\item \texttt{Model\_clustering.pdf}: Plot of unsupervised hierarchical clustering of regression models.
\item \texttt{Agg\_errors.pdf}: Plot showing the score distribution and cutoff used for determination of candidate regulators 
\item \texttt{Candidate\_regulators.txt}: Tab separated file with subtype specific and common candidate regulators and their predicted direction of regulation. The subtype regulators are sorted based on their scores.
\end{enumerate}

\subsection {Commands}
Creating \texttt{sample\_inputs.rda} from the sample input files\linebreak
\texttt{ create.inputs ('annotations.txt', 'gene\_expression.txt', list(TFmiRNAs='tfs\_mirs.txt'), NULL, list (CopyNumbers='copy\_numbers.txt'), 'sample\_inputs.rda')}
 \linebreak \linebreak
Command for determining sample-by-sample models. \linebreak
\texttt{sbs.run.regulator.inference ('sample\_inputs.rda', '.', no.threads=3)}


\end{document}